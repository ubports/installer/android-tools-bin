# Changelog

This changelog is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

android-tools-bin versioning started at 1.0.0, but this changelog was not added until 1.0.5.

## [Unreleased]

## [2.0.1] - 2022-11-28

### Added

- Bring back CommonJS

### Updated

- Updated dependencies

## [2.0.0] - 2022-10-24

### Changed

- Moved to typescript

### Fixed

- Gitlab CI Cobertura coverage

### Security

- significantly reduced number of dependencies

## [1.0.8] - 2022-09-11

### Changed

- Updated binaries and dependencies

| tool       | version |
| ---------- | ------- |
| adb        | 33.0.3  |
| fastboot   | 33.0.3  |
| heimdall   | 1.4.2   |
| make_f2fs  | 1.15.0  |
| sload_f2fs | 1.15.0  |
| sqlite3    | 3.32.2  |

## [1.0.7] - 2022-06-24

### Changed

- Updated binaries, dependencies, and metadata

## [1.0.6] - 2022-03-18

### Changed

- Updated binaries, dependencies, and metadata
- Switched to prettier from eslint

## [1.0.5] - 2021-04-16

### Fixed

- Fixed Heimdall on Windows by adding MinGW32 build of libusb to win32/x86/ directory ([#1](https://gitlab.com/ubports/installer/android-tools-bin/-/issues/1))
