# android-tools-bin

[![ci](https://gitlab.com/ubports/installer/android-tools-bin/badges/master/pipeline.svg)](https://gitlab.com/ubports/installer/android-tools-bin/) [![coverage report](https://gitlab.com/ubports/installer/android-tools-bin/badges/master/coverage.svg)](https://ubports.gitlab.io/installer/android-tools-bin/coverage/) [![npm](https://img.shields.io/npm/v/android-tools-bin)](https://www.npmjs.com/package/android-tools-bin) [![docs](https://img.shields.io/badge/docs-built-green)](https://ubports.gitlab.io/installer/android-tools-bin/)

An npm package with pre-built binaries for android sdk tools such as `adb`, `fastboot`, `mke2fs` and `heimdall`. A convenience function helps finding the correct executable and environment variables and config options are available to manipulate the behavior.

See [promise-android-tools](https://www.npmjs.com/package/promise-android-tools) for a full wrapper that provides async access to all tools and subcommands.

## Usage

Install the library by running `npm i android-tools-bin`. See the [documentation](https://ubports.gitlab.io/installer/android-tools-bin/) for detailed API information.

### Optimistic detection

By default, the function will check if the requested tool is available for the current architecture and platform and fall back to returning a system command for the natively installed tool, if none is packaged.

```javascript
import { getAndroidToolPath } from "android-tools-bin";
const adbCommand = getAndroidToolPath("adb");
const fastbootCommand = getAndroidToolPath("fastboot");
const mke2fsCommand = getAndroidToolPath("mke2fs");
const heimdallCommand = getAndroidToolPath("heimdall");
```

### Pessimistic detection

To prevent falling back to native system commands, pass `false` as the second argument. The function will then throw an error if the requested binary is not available.

```javascript
import { getAndroidToolPath } from "android-tools-bin";
try {
  const adbCommand = getAndroidToolPath("adb", false);
} catch (error) {
  console.log(
    "they told me choosing PowerPC architecture in the 21st century was a bad idea, but i just wouldn't listen..."
  );
}
```

### Requesting native tools using environment variables

To allow the user of a packaged NodeJS or Electron application some level of control as to wether the native tools should be used, the function provides environment variables:

```sh
USE_SYSTEM_TOOLS # if specified, no packaged binaries will be used
USE_SYSTEM_<tool> # pass the toolname in all caps (eg. USE_SYSTEM_ADB) to select which tools to use natively
```

### Requesting native tools using the native argument

If, for some reason, you don't want to use the packaged binaries, you can use the third argument to pass an object describing which binaries you want natively and which you want packaged. You can also specify to not use any packaged binaries:

```javascript
import { getAndroidToolPath } from "android-tools-bin";
const adbCommand = getAndroidPlatformTools("adb", true, { adb: true });
const fastbootCommand = getAndroidPlatformTools("fastboot", true, {
  all: true
});
const fastbootCommand = getAndroidPlatformTools("fastboot", true, {
  heimdall: true,
  mke2fs: true
});
```

### Base directory

You can get the base directory of the packaged files by calling `getAndroidToolBaseDir()`.

### Documentation

Documentation for the latest release available [online](https://ubports.gitlab.io/installer/android-tools-bin/). You can build it yourself by running `npm run docs`. Then, open `./docs/android-tools-bin/1.0.0/index.html` in [your favorite browser](https://www.mozilla.org/en-US/firefox/).

## Development

```bash
$ npm install # to install dependencies
$ npm update # to update dependencies
$ npm audit fix # to install security patches
$ npm run lint # to automatically fix coding style issues
$ npm run test # to run unit-tests with coverage reports
$ npm run docs # to build detailed jsdoc documentation
$ npm run download # to download binaries
$ npm run build # to compile the typescript code
```

## History

The library was originally developed for use in the [UBports Installer](https://github.com/ubports/ubports-installer) and its [promise-android-tools](https://www.npmjs.com/package/promise-android-tools) library, but it might be useful to other projects. Semantic versioning will ensure API stability for public functions.

## License

Original development by [Johannah Sprinz](https://spri.nz). Copyright (C) 2020-2022 [UBports Foundation](https://ubports.com).

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.

## Tools

- The [Android SDK Platform Tools](https://developer.android.com/studio/releases/platform-tools) (such as adb and fastboot) are [Apache](https://android.googlesource.com/platform/system/adb/+/refs/heads/master/NOTICE)-licensed universal Android utilities
- [Heimdall](https://gitlab.com/BenjaminDobell/Heimdall/) is an [MIT](https://gitlab.com/BenjaminDobell/Heimdall/-/blob/master/LICENSE)-licensed replacement for the leaked ODIN tool to flash Samsung devices.
